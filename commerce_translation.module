<?php

/**
 * @file
 * The commerce_translation main module file.
 */

/**
 * Implements hook_preprocess_menu_link().
 *
 * Make the theme_menu_link() callback easily support multilingual menu items
 * for nodes with entity translation enabled.
 */
function commerce_translation_preprocess_menu_link(&$variables) {
  if (function_exists('i18n_string_translate')) {
    // Force menu links to be translated if the menu title is translated.
    $variables['element']['#title'] = i18n_string_translate("menu:item:{$variables['element']['#original_link']['mlid']}:title", $variables['element']['#title'], array('sanitize' => FALSE));
    $variables['element']['#original_link']['localized_options']['alter'] = TRUE;
    $variables['element']['#original_link']['options']['alter'] = TRUE;
    $variables['element']['#localized_options']['alter'] = TRUE;
  }
}

/**
 * Implements hook_i18n_string_list().
 */
function commerce_translation_i18n_string_list($group) {
  $strings = array();
  if ($group == 'commerce') {
    // Allow the commerce_product bundles to be translated.
    if (function_exists('commerce_product_types')) {
      $product_types = commerce_product_types();
      foreach ($product_types as $bundle => $product_type) {
        $strings['commerce']['product'][$bundle]['name'] = $product_type['name'];
      }
    }

    // Allow the Fee and Tax price components to be translated.
    if (function_exists('commerce_price_component_types')) {
      $component_types = commerce_price_component_types();
      foreach ($component_types as $machine_name => $component_type) {
        if (isset($component_type['fee_type']) || isset($component_type['tax_rate'])) {
          if (!empty($component_type['display_title'])) {
            $strings['commerce']['price_component'][$machine_name]['display_title'] = $component_type['display_title'];
          }
        }
      }
    }

    // Allow payment methods to be translated.
    if (function_exists('commerce_payment_methods')) {
      $payment_methods = commerce_payment_methods();
      foreach ($payment_methods as $machine_name => &$payment_method) {
        $strings['commerce']['payment_method'][$machine_name]['title'] = $payment_method['title'];
        if (isset($payment_method['display_title'])) {
          $strings['commerce']['payment_method'][$machine_name]['display_title'] = $payment_method['display_title'];
        }
        if (isset($payment_method['short_title'])) {
          $strings['commerce']['payment_method'][$machine_name]['short_title'] = $payment_method['short_title'];
        }
      }
    }

    // Allow the shipping services to be translated.
    if (function_exists('commerce_shipping_services')) {
      $services = commerce_shipping_services();
      foreach ($services as $machine_name => $service) {
        if (isset($service['display_title'])) {
          $strings['commerce']['shipping_service'][$machine_name]['display_title'] = $service['display_title'];
        }
      }
    }

    // Allow the custom order statuses to be translated.
    if (function_exists('commerce_custom_order_status_commerce_order_status_info')) {
      $custom_order_statuses = commerce_custom_order_status_commerce_order_status_info();
      foreach ($custom_order_statuses as $machine_name => $order_status) {
        $strings['commerce']['custom_order_status'][$machine_name]['title'] = $order_status['title'];
      }
    }
  }
  return $strings;
}

/**
 * Implements hook_commerce_product_type_info_alter().
 *
 * Make product types translatable. Product types have to be written in English
 * in the UI.
 */
function commerce_translation_commerce_product_type_info_alter(&$product_types) {
  foreach ($product_types as $bundle => &$product_type) {
    $product_type['name'] = commerce_i18n_string("commerce:product:{$bundle}:name", $product_type['name'], array('sanitize' => FALSE));
  }
}

/**
 * Implements hook_commerce_shipping_service_info_alter().
 */
function commerce_translation_commerce_shipping_service_info_alter(&$shipping_services) {
  foreach ($shipping_services as $machine_name => &$shipping_service) {
    if (isset($shipping_service['display_title'])) {
      $shipping_service['display_title'] = commerce_i18n_string("commerce:shipping_service:{$machine_name}:display_title", $shipping_service['display_title'], array('sanitize' => FALSE));
    }
  }
}

/**
 * Implements hook_commerce_price_component_type_info_alter().
 */
function commerce_translation_commerce_price_component_type_info_alter(&$component_types) {
  foreach ($component_types as $machine_name => &$component_type) {
    if (isset($component_type['fee_type']) || isset($component_type['tax_rate'])) {
      if (!empty($component_type['display_title'])) {
        $component_type['display_title'] = commerce_i18n_string("commerce:price_component:{$machine_name}:display_title", $component_type['display_title'], array('sanitize' => FALSE));
      }
    }
  }
}

/**
 * Implements hook_commerce_order_status_info_alter().
 */
function commerce_translation_commerce_order_status_info_alter(&$order_statuses) {
  if (function_exists('commerce_custom_order_status_commerce_order_status_info')) {
    $custom_order_statuses = commerce_custom_order_status_commerce_order_status_info();
    foreach ($custom_order_statuses as $machine_name => $order_status) {
      $order_statuses[$machine_name]['title'] = commerce_i18n_string("commerce:custom_order_status:{$machine_name}:title", $order_statuses[$machine_name]['title'], array('sanitize' => FALSE));
    }
  }
}

/**
 * Implements hook_commerce_payment_method_info_alter().
 */
function commerce_translation_commerce_payment_method_info_alter(&$payment_methods) {
  foreach ($payment_methods as $machine_name => &$payment_method) {
    $payment_method['title'] = commerce_i18n_string("commerce:payment_method:{$machine_name}:title", $payment_method['title'], array('sanitize' => FALSE));
    if (isset($payment_method['display_title'])) {
      $payment_method['display_title'] = commerce_i18n_string("commerce:payment_method:{$machine_name}:display_title", $payment_method['display_title'], array('sanitize' => FALSE));
    }
    if (isset($payment_method['short_title'])) {
      $payment_method['short_title'] = commerce_i18n_string("commerce:payment_method:{$machine_name}:short_title", $payment_method['short_title'], array('sanitize' => FALSE));
    }
  }
}
